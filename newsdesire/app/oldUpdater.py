import random
from app.models import *
from pyvirtualdisplay import Display
from selenium import webdriver
from sys import stderr
from time import sleep
from datetime import datetime
import string
from app.topic_tracker import freezer

# with Display():
while True:
    browser = webdriver.Chrome("C:/Users/olfre/Documents/Work/News Desire/ndwebapp/newsdesire/chromedriver.exe")
    print("# Starting at {}#".format(datetime.now()))
    freezer()
    # "news": "nws", "images":"isch", "videos":"vid"
    params = { "news": "nws",}
    param_content = None
    param_page = 0
    for p in params:
        if p == "news":
            param_content = "&tbm=nws&tbs=qdr:d,sbd:1"
            param_page = 10
        if p == "images":
            param_page = 0
            param_content = "&tbm=isch&tbs=qdr:d"
        if p == "videos":
            param_page = 20
            param_content = "&tbm=vid&tbs=qdr:d"
        while param_page > -1 :
            print("Tracking {} with page param at {}".format(p, param_page))
            for ss in Search_String.objects.filter(is_frozen=False):
                search_string_list = []
                browser.get('https://www.google.com/search?q=' + ss.string.replace(" ", "+") + param_content + "&start=" + str(param_page))
                browser.page_source
                assert False
                if p == "news":
                    google_gs = browser.find_elements_by_class_name("g")
                    print("Got {1} updates for the string {0}".format(ss.string, len(google_gs)))
                    # if len(google_gs) == 0:
                    #     assert False
                    for srg in google_gs:
                        try:
                            new_update = Update(search_string_id=ss.id)
                            # get title
                            element = srg.find_element_by_tag_name("h3")
                            element = element.find_element_by_tag_name("a")
                            new_update.title = element.get_attribute('innerHTML').replace("<em>","").replace("</em>","")
                            element = element.find_element_by_tag_name("a")
                            new_update.title = element.get_attribute('innerHTML')
                            # get URL
                            element = srg.find_element_by_tag_name("a")
                            new_update.url = element.get_attribute('href')
                            if Update.objects.filter(url=new_update.url).exists():
                                continue
                            # get text
                            element = srg.find_element_by_class_name("st")
                            new_update.text = element.get_attribute('innerHTML')
                            key_id = ""
                            for i in range(10):
                                key_id += random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase)
                            new_update.key_id = key_id
                            new_update.save()
                        except:
                            pass
            param_page -= 10
    browser.quit()
    print("# Ended at {}".format(datetime.now()))
    break

print("Done")