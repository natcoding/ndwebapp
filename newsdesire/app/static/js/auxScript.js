/* script that need to be present before the page loads */
let message_next_feature = "Thsi feature will be available from the next update. If you do not find it useful, please drop me a line.";
var reachedBottomOfPage = false;

function showSavedUpdates() {
    $.ajax({
        type: "GET",
        url: "/update/get/saved/",
        success: function (result) {
            toggleContent("saved");
            if (result == "void") {
                document.querySelector("#saved").innerHTML = `<p class="muted-text">You have't saved anything yet!</p>`;
            }
            else {
                document.querySelector("#saved").innerHTML = result;
            }
        },
        error: function () {
            $(".fixed-top").notify("Something went wrong. We could not retrieve your saved updates", { position: "bottom center", className: 'error' });
            toggleContent("updates");
        }

    });
}

function toggleContent(c) {
    // toggle between middle contents
    let contents = ["updates", "saved", "topics", "topic-specific-updates", "settings"]
    for (let i = 0; i < contents.length; i++) {
        document.querySelector(`#${contents[i]}`).style.display = "none";
    }

    //Make container visible
    document.querySelector(`#${c}`).style.display = "block";
    document.querySelector(`#left-content`).style.display = "block";

    if (c != "updates" && c != "settings") {
        document.querySelector(`#${c}`).innerHTML = `<div class="mx-auto mt-5" id="loader-spiner""><i class='fas fa-spinner fa-w-16 fa-spin fa-lg'></i> loading your ${c}...</div>`;
        window.scrollTo(0,0);
    }
    if (c == "settings") {
        getSettings();
        document.querySelector(`#left-content`).style.display = "none";
    }
}

function showNewTopicForm(){
    if(document.querySelector("#nav-btn-grp").style.display == "")
    {
        document.querySelector("#nav-btn-grp").style.display = "none";
        document.querySelector("#new-string-form").style.display = "block";
        document.querySelector("#new-string-form").classList.remove("d-none");
        document.querySelector("#new-string-form").classList.remove("d-md-block");
    }
    else{
        document.querySelector("#new-string-form").style.display = "none";
        document.querySelector("#new-string-form").classList.add("d-none");
        document.querySelector("#new-string-form").classList.add("d-md-block");
        document.querySelector("#nav-btn-grp").style.display = "";
    }
}

function readUpdate(key_id) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/update/read/" + key_id,
            success: function (result) {
                window.open(result)
                btn = document.querySelector("#btn-read-" + key_id);
                if (btn.innerHTML.indexOf('Again') == -1)
                    btn.innerHTML = "Read Again";
            },
            error: function (message) {
                $(".fixed-top").notify(message, { position: "bottom center", className: 'error' });
            },
        });
    })(jQuery)
}

function saveUpdate(key_id) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/update/save/" + key_id,
            success: function (result) {
                if (result.length == 0) {
                    $(`#${key_id}-btn-save`).notify("Update successfully saved.", { position: "right middle", className: 'success' });
                    document.getElementById(`${key_id}-btn-save`).innerText = "remove";
                    document.getElementById(`${key_id}-btn-save`).setAttribute("onclick", `unsaveUpdate("${key_id}");`);
                }
                else {
                    $(`#${key_id}-btn-save`).notify(result, { position: "right middle", className: 'info' });
                    //
                }
            },
            error: function (message) {
                $(`#${key_id}-btn-save`).notify("Could not save the update. Please try again later", { position: "left middle", className: 'error' });
            },
        });
    })(jQuery)
}

function likeUpdate(key_id) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/update/like/" + key_id,
            success: function (result) {
                hearts = document.querySelectorAll("#heart-" + key_id);
                likeCount = document.getElementById(`${key_id}-like-count`);
                for (let i = 0; i < hearts.length; i++) {
                    if (hearts[i].innerHTML.indexOf("fas") != -1) {
                        hearts[i].innerHTML = "<i class='far fa-heart'></i>";
                        likeCount.innerText = parseInt(likeCount.innerText) - 1;
                    }
                    else {
                        hearts[i].innerHTML = "<i class='fas fa-heart'></i>";
                        likeCount.innerText = parseInt(likeCount.innerText) + 1;
                    }
                }
            },
            error: function (message) {
                $("#heart-" + key_id).notify("Operation failed", { position: "left middle", className: 'error' });
            },
        });
    })(jQuery)
}

function unsaveUpdate(key_id) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/update/unsave/" + key_id,
            success: function (result) {
                if (document.querySelector("#saved").style.display != "none") {
                    let card = document.querySelector("#card-" + key_id + "-saved");
                    let parent_div = card.parentNode
                    card.parentNode.removeChild(card);
                    if (parent_div.childElementCount == 0) {
                        parent_div.innerHTML = `<div class="mx-auto" id="loader-spiner" style="width: 200px;"><p class="muted-text">All clear here! <i class="fas fa-thumbs-up"></i></p></div>`;
                    }
                }
                $(`#${key_id}-btn-save`).notify("Update successfully removed.", { position: "left middle", className: 'success' });
                document.getElementById(`${key_id}-btn-save`).innerText = "save";
                document.getElementById(`${key_id}-btn-save`).setAttribute("onclick", `saveUpdate("${key_id}");`);
            },
            error: function (message) {
                $(`#${key_id}-btn-save`).notify("Something wrong occured. Update not removed", { position: "left middle", className: 'error' });
            },
        });
    })(jQuery)
}

function removeInterst(topic, cardID) {
    let cardSender = document.getElementById(cardID);
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/interest/mine/remove/" + topic,
            success: function (result) {

                /*  The following is only done if we're on the updates div, else, we'd be
                    on the topics div.
                */
                if (document.querySelector("#updates").style.display != "none") {
                    let hasElement = true;
                    allCards = document.getElementsByClassName("my-2");
                    let cardToScrollTo;
                    for (let i = 0; i <= allCards.length; i++) {
                        if (allCards[i] == cardSender) {
                            if (i != 0 && (i + 1 != allCards.length)) {
                                for (let a = i + 1; a <= allCards.length; a++) {
                                    // find the next card with a different topic
                                    if (allCards[a].attributes.name.value != cardSender.attributes.name.value) {
                                        cardToScrollTo = allCards[a]; break;
                                    }
                                }
                            }
                            else { break; }
                        }
                    }

                    while (hasElement) {
                        if (document.getElementsByName(topic)[0]) {
                            card = document.getElementsByName(topic)[0]
                            card.parentNode.removeChild(card);
                        } else {
                            hasElement = false;
                        }
                    }

                    if (cardToScrollTo) { cardToScrollTo.scrollIntoView(); }
                    else { window.scrollTo(0, document.body.scrollHeight); }
                }
                else if (document.querySelector("#topics")) {
                    cardSender.parentNode.removeChild(cardSender);
                }
                $(".fixed-top").notify("Topic successfully removed from your interests.", { position: "bottom center", className: 'success' });
            },
            error: function () {
                $(".fixed-top").notify("Something wrong occured. The topic was not removed", { position: "bottom center", className: 'error' });
            },
        });
    })(jQuery)
}


function pauseAllOtherTopic(topicID) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/interst/pause-all-other/${topicID}/`,
            success: function (result) {
                $(".fixed-top").notify("You will only receive updates on this topic from now on until you unpause the others.", { className: "warning", position: "bottom center" });
            },
            error: function () {
                $(".fixed-top").notify("Something went wrong", { className: "error", position: "bottom center" });
            }

        });
    })(jQuery)
}


function pauseTopic(topicID) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/interst/pause/${topicID}/`,
            success: function (result) {
                if (document.querySelector(`#topics-container`)) { showMyTopics(); }
                else { togglePauseUnpause(topicID); }
                $.notify("You will not receive updates on this topic anymore until you unpause it.", { className: "warning", position: "top center" });
            },
            error: function (message) {
                if (message) $(".fixed-top").notify(message, { className: "error", position: "bottom center" });
                else { $(".fixed-top").notify("Something went wrong", { className: "error", position: "bottom center" }); }

            }

        });
    })(jQuery)
}

function unpauseTopic(topicID) {
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/interst/unpause/${topicID}/`,
            success: function (result) {
                if (document.querySelector(`#topics-container`)) { showMyTopics(); }
                else { togglePauseUnpause(topicID); }
                $.notify("Receiving updates about this topic has resumed", { className: "success", position: "top center" });
            },
            error: function () {
                $(".fixed-top").notify("Something went wrong", { className: "error", position: "bottom center" });
            }

        });
    })(jQuery)
}

function togglePauseUnpause(topicID) {
    links = document.querySelectorAll("#lnk-pause-" + topicID);
    let text = "pause this topic"

    if (links[0].innerText == text) {
        text = "unpause this topic";
    }

    for (let i = 0; i < links.length; i++) {
        links[i].innerText = text;
        if (text == "pause this topic")
            links[i].setAttribute("onclick", `pauseTopic(${topicID});`);
        else {
            links[i].setAttribute("onclick", `unpauseTopic(${topicID});`);
        }
    }

}

function displayTotalUpdateSavedHot(t) {
    let _url;
    let obj;
    if (t == "hot") {
        _url = "/update/count/hot/";
        obj = document.querySelector("#count-hot-updates");
    }
    else {
        _url = "/update/count/saved/";
        obj = document.querySelector("#count-saved-updates");
    }
    $.ajax({
        type: "GET",
        url: _url,
        success: function (count) {
            let objVal = obj.innerHTML;
            if (parseInt(objVal) == NaN) {
                obj.innerHTML = count;
                // obj2.innerHTML = count;
            }
            else {
                if (objVal != count) {
                    obj.innerHTML = count;
                    // obj2.innerHTML = count;
                }
            }
        }
    });
}

$(window).on('load', function () {
    setInterval(function () { /*displayTotalUpdateSavedHot("hot");*/ displayTotalUpdateSavedHot("saved"); }, 2500);
});