
function showMyTopics() {
    toggleContent("topics");
    (function ($) {
        $.ajax({
            type: "GET",
            url: "/interst/mine/get/all",
            success: function (result) {
                document.querySelector(`#topics`).innerHTML = `<div id="topics-container"></div>`;
                document.querySelector(`#topics-container`).classList.add("flex-container");
                document.querySelector("#topics-container").innerHTML = result;
            },
            error: function () {
                $(".fixed-top").notify("Something went wrong. We could not retrieve your topics", { position: "bottom center", className: 'error' });
                toggleContent("updates");
            }

        });
    })(jQuery)
}

function viewTopicsUpdates(topic, label) {
    /* label can be all, saved, favorite or read */
    document.querySelector("#topics-container").innerHTML = `<div class="mx-auto" id="loader-spiner" style="width: 200px;">
                                                        <i class='fas fa-spinner fa-w-16 fa-spin fa-lg'></i>
                                                    </div>`;
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/update/get/topic/${topic}/labelled/${label}`,
            success: function (result) {
                if (result == " ") {
                    document.querySelector("#topics-container").innerHTML = `<div style="inline-block">Ooops! Nothing to see here anymore <i class="far fa-sad-tear"></i></div>`;
                }
                else {
                    document.querySelector("#topics-container").innerHTML = result;
                }
            },
            error: function () {
                document.querySelector("#topics-container").innerHTML = "Something went wrong. We could not retrieve your updates.&nbsp;<a href='#' onclick='showMyTopics()'>Return to list of topics</a>";
            }

        });
    })(jQuery)
}

function removeTopicUpdates(topicID, label) {
    /*  
    This function removes saved or favorite updates from the user's list
    label can be favorite or saved
    */
    obj = document.querySelector(`#topic-${topicID}-${label}`)
    obj.innerHTML = `<i class='fas fa-spinner fa-w-16 fa-spin fa-lg'></i>`;
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/update/remove/topic/${topicID}/labelled/${label}`,
            success: function (result) {
                obj.innerHTML = 0;
            },
            error: function () {
                $(".fixed-top").notify("Something went wrong. We could not retrieve your updates", { className: "error", position: "bottom center" });
            }

        });
    })(jQuery)
}


function followTopic(topicID) {
    /*  
    This allows hot tracking: tracking a topic not by typing its name
    but by clicking on it if the follow button is there.
    */
    btnSender = document.querySelector(`#btn-topic${topicID}-follow`);
    (function ($) {
        text = "follow";
        _url = ""
        if (btnSender.innerText == "follow this") {
            text = "unfollow";
            _url = `/interst/flw/${topicID}/`;
        }
        else {
            text = "follow this";
            _url = `/interst/uflw/${topicID}/`;
        }
        $.ajax({
            type: "GET",
            url: _url,
            success: function (result) {
                $(`#btn-topic${topicID}-follow`).notify(result, { className: "success", position: "right middle" });
                btnSender.innerText = text;
            },
            error: function () {
                $(`#btn-topic${topicID}-follow`).notify("Something went wrong. Please try again", { className: "right", position: "left middle" });
            }

        });
        // .setAttribute("onclick", `saveUpdate("${key_id}");`);
    })(jQuery)
}

function getTopicSpecificUpdate(topicID, q) {
    // Gets q updates from the specific with id topicID
    toggleContent("topic-specific-updates");
    (function ($) {
        $.ajax({
            type: "GET",
            url: `/update/get/specific/${topicID}/${q}/`,
            success: function (result) {
                container = document.querySelector("#topic-specific-updates");
                container.innerHTML = result;
            },
            error: function () {
                $(".fixed-top").notify("Something went wrong, couldn't collect updates", "error", { position: "bottom center" });
            }
        });
    })(jQuery)
}


