let topics;
(function ($) {
    $("#submit-new-string").click(function () {
        var data = new FormData(document.querySelector("#new-string-form"));
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            cache: false,
            url: "/search-string/new/",
            data: data,
            success: function (result) {
                if (result.length == 0) {
                    $(".fixed-top").notify("Topic successfully saved for tracking.", { position: "bottom center", className: 'success' });
                    $("#new-string-form").trigger('reset');
                }
                else {
                    $(".fixed-top").notify(result, { position: "bottom center", className: 'info' });
                }
                // $(".fixed-top").trigger('reset');
                if (document.querySelector("#topics").style.display != "none") {
                    showMyTopics();
                }
            },
            error: function (message) {
                $(".fixed-top").notify("Something went wrong, topic not saved", { position: "bottom center", className: 'error' });
            }

        });
    });
    
    $("#cancel-new-string").click(function () {showNewTopicForm()});

    function getAllTopics() {
        $.ajax({
            type: "GET",
            url: "/interst/get/all",
            success: function (data) {
                topics = data;
            }
        });
    }

    // Preload topics
    $(window).on('load', function () {
        getAllTopics();
        setInterval(getAllTopics, 5000);
    })

    $("#search-string-txt").on('input', function () {
        a = document.querySelector("#search-string-txt").value;
        if (a.length > 2) {
            list = document.querySelector("#topics-sg");
            while (list.children.length != 0) { 
                e = list.children[list.children.length - 1];
                e.parentNode.removeChild(e);
             }
            topics.forEach(function (item, index, topics) {

                if (item.includes(a)) {
                    sg = `<option value="${item}"></option>`;
                    for (let i = 0; i < list.children.length; i++) {
                        if (sg == list.children[i]) continue
                    }
                    list.insertAdjacentHTML("afterbegin", `<option value="${item}">`);
                }

            });
        }
    });
})(jQuery)
