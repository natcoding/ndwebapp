
(function ($) {
    $('#btn-sign-up').on('click', function (e) {
        form = document.querySelector("#frm-auth");
        var data = new FormData(form);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            cache: false,
            url: "/usr/register/",
            data: data,
            success: function (result) {
                location.href='/dashboard/';
            },
            error: function (message) {
                $(".fixed-top").notify(message.responseText,{ position: "bottom center", className: 'error' });
            }
        
        });
        e.preventDefault();
    });

    $('#btn-sign-in').on('click', function (e) {
        form = document.querySelector("#frm-auth");
        var data = new FormData(form);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            cache: false,
            url: "/",
            data: data,
            success: function (result) {
                // $.notify('',);
            },
            error: function (message) {
                $(".fixed-top").notify("Something went wrong. Please make sure your login details are correct.",{ position: "bottom center", className: 'error' });
            }
        
        });
    });
})(jQuery)