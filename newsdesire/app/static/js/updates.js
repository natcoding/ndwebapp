(function ($) {
    let hasCalledLoadMore = false;
    let btnLoadLatestIsPresent = false;
    let unloadedUpdateCount = 0;
    display = document.querySelector("#updates");
    defCard = `<div class='my-2' id='defCard'>
                    <div class='card'>
                        <div class='card-body'>
                            <h5 class='card-title'>Welcome again,</h5>
                            <p class='card-text txt-justified'>You are seing this because you either have not yet added any topic of interest or we have not got news for you.<br><br>
                                If you have not added a topic yet, you can do so by filling in the form at the top. Just type in whatever you wish to be informed about such as your city's name, a personality, a company
                                and so on.
                            </p>
                            <p>If you have already added a topic or more, very soon you'll start receiving updates. The more topic you add, the more news you get</p>
                            <p class="txt-justified">Once you have added topics, as soon as there is an update over the internet, you will receive it right here.</p>
                            <br>
                            <p>You can refresh the page <a href="/">here</a> if you feel unpatient <i class="far fa-grin-beam-sweat"></i>
                            <p class="text-muted txt-justified">Stay tuned!</p>
                        </div>
                    </div>
                </div>`;
    // Collects newly saved updates from the logged user's search strings
    function loadLatest(p = "None") {
        _url = "/update/get/latest/";
        if (p == "more") _url = "/update/get/newest/";
        $.ajax({
            type: "GET",
            url: _url,
            success: function (result) {
                if (result != "None") {
                    if (document.querySelector("#defCard")) {
                        crd = document.querySelector('#defCard')
                        display.removeChild(crd);
                    }
                    if (display.childElementCount == 0) {
                        display.insertAdjacentHTML('afterbegin', result);
                    }
                    else {
                        unloadedUpdateCount = refreshUnloadedUpdateCount(result);
                        if (document.querySelector('#btn-load-latest')) document.querySelector('#btn-load-latest').innerHTML = `Load new updates (${unloadedUpdateCount})`;
                        // document.querySelector('#unloaded-count-brand').innerHTML = `${unloadedUpdateCount}`;
                        document.title = `(${unloadedUpdateCount}) Floowing`;
                        document.querySelector("#unloaded-count-updates").innerHTML = `${unloadedUpdateCount}`; // latest button top
                        document.querySelector("#unloaded-count-updates2").innerHTML = `${unloadedUpdateCount}`;
                        if (p == "more") {
                            display.insertAdjacentHTML('afterbegin', result);
                            if (document.querySelector("#btn-load-latest")) {
                                btn = document.querySelector('#btn-load-latest')
                                display.removeChild(btn);
                                document.title = "Floowing";
                                // document.querySelector('#unloaded-count-brand').innerHTML = "";
                                document.querySelector("#unloaded-count-updates").innerHTML = ""; // latest button top
                                document.querySelector("#unloaded-count-updates2").innerHTML = "";
                            }
                        }
                        else if (!document.querySelector("#btn-load-latest")) {
                            let btn = `<button type="button" class="btn btn-outline-primary btn-sm btn-block mt-2" id="btn-load-latest">Load new updates (${unloadedUpdateCount})</button>`;
                            display.insertAdjacentHTML('afterbegin', btn);
                            btnLoadLatestIsPresent = true;
                            $('#btn-load-latest').on('click', function () {
                                btn = document.querySelector('#btn-load-latest')
                                btn.innerHTML = "<i class='fas fa-spinner fa-w-16 fa-spin fa-lg'></i>"
                                document.title = "Floowing";
                                // document.querySelector('#unloaded-count-brand').innerHTML = "";
                                document.querySelector("#unloaded-count-updates").innerHTML = ""; // latest button top
                                document.querySelector("#unloaded-count-updates2").innerHTML = "";
                                loadLatest("more");
                            });
                        }
                    }
                } else {
                    if (!document.querySelector("#defCard") && display.childElementCount == 0) {
                        display.insertAdjacentHTML('afterbegin', defCard);
                    }
                }
                // else {
                //     if (btnLoadLatestIsPresent) {
                //         btn = document.querySelector('#btn-load-latest')
                //         display.removeChild(btn);
                //         btnLoadLatestIsPresent = false;
                //     }
                // }
            },
            error: function (message) {
                if(btnLoadLatestIsPresent == true)
                    $(".fixed-top").notify("Something went wrong, couldn't collect latest news updates", "error", { position: "bottom center" });
            }
        });
    }

    function load_more() {
        if (document.querySelector("#updates").style.display == "block") {
            hasCalledLoadMore = true;
            $.ajax({
                type: "GET",
                url: "/update/get/more/",
                success: function (result) {
                    document.querySelector("#loader-spiner").style.display = "none";
                    if (result != "None") {
                        display = document.querySelector("#updates");
                        display.insertAdjacentHTML('beforeEnd', result);
                        hasCalledLoadMore = false;
                    } else {
                        $.notify("No more old news", { position: "bottom center", className: 'info' });
                    }
                },
                error: function (message) {
                    $.notify("Something went wrong! Couldn't collect more news updates", { position: "bottom center", className: 'error' });
                    hasCalledLoadMore = false;
                }
            });
        }else{
            document.querySelector("#loader-spiner").style.display = "none";
        }
    }

    function refreshUnloadedUpdateCount(result) {
        let n = 0;
        for (let i = 0; i < result.length; i++) {
            a = result.slice(i, i + 2);
            if (a == "my") {
                n++;
            }
        }
        return (n);
    }

    function UpdateTrending(){
        $.ajax({
            type: "GET",
            url: "/interst/trending/",
            success: function(result){
                container = document.querySelector("#trending-topics");
                if(container.innerHTML != result) {
                    container.innerHTML = result;
                }
            }
        });
    }

    function UpdatePopular(){
        $.ajax({
            type: "GET",
            url: "/interst/popular/",
            success: function(result){
                container = document.querySelector("#popular-topics");
                if(container.innerHTML != result) {
                    container.innerHTML = result;
                }
            }
        });
    }


    $(window).on('load', function () {
        toggleContent("updates");
        loadLatest();
        UpdateTrending();
        UpdatePopular();
        setInterval(function () { loadLatest(); UpdateTrending();UpdatePopular();}, 10000);
    });

    $('#btn-load-more').on('click', function () {
        load_more();btn-menu
    });

    $('#btn-menu').on('click', function () {
        btnSignOut = document.querySelector("#nav-btn-grp");
        if(btnSignOut.style.display == "none"){
            btnSignOut.style.display = "block";
        }
        else{
            btnSignOut.style.display = "none"
        }
    });

    $(window).scroll(function () {
        // Infinite scroll
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            reachedBottomOfPage = true;
            if (!hasCalledLoadMore && !document.querySelector("#defCard")) {
                document.querySelector("#loader-spiner").style.display = "block";
                load_more();
            }
        }else{
            reachedBottomOfPage = false;
        }
        // Make left-content stick to screen
    });

})(jQuery)