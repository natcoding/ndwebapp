let settings = undefined;
function getSettings(){
    $.ajax({
        type:"GET",
        url:"/settings/get/all",
        success:function(result){
            settings = result;
            for(let setting in settings){
                obj = document.querySelector(`#${setting}`)
                obj.value = result["pref_lang"];
            }
        },
        error:function(){
            $(".fixed-top").notify("We could not collect your settings", "error", { position: "bottom center" });
        }
    })
}

function updateSettings(type) {
    (function ($) {
        data = {[type]:document.querySelector(`#${type}`).value};
        $.ajax({
            type:"POST",
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url:"/settings/update/",
            data:data,
            success:function(){
                $(`#${type}`).notify(
                    "Settings succesfully updated", 
                    { className:"success", position:"left middle" }
                  );
            },
            error:function(){
                $(`#${type}`).notify(
                    "Updating settings failed...", 
                    { className:"error", position:"left middle" }
                  );
            },
            complete:function(){
            }
        })
    })(jQuery)
}