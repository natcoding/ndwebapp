# coding=utf-8
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from app.models import *
from django.template import Template, Context
from datetime import datetime
from django.http import HttpResponseServerError, JsonResponse
from app.views.dashboard import reload_user_settings

topics_cards = Template("\
    {% for topic,data in data.items %}\
        <div id='topic-{{ data.id }}' class='col-xs-12 col-sm-8 col-md-8 col-lg-6 col-xl-6 mt-2 flex-item'>\
                    <div class='card'>\
                        <div class='card-header h-topic'>\
                            <h6>{{ topic }}</h6>{% if data.id|safe in request.session.paused_topics %}&nbsp;<span class='badge badge-secondary'>paused</span>{% endif %}\
                            <i class='fas fa-cog' id='menu-topic-{{ data.id }}' data-toggle='dropdown' aria-haspopup='true'></i>\
                            <div class='dropdown-menu dropdown-menu-left' aria-labelledby='menu-topic-{{ data.id }}'>\
                                            <a class='dropdown-item'  onclick=\"removeTopicUpdates({{ data.id }}, 'saved');\">unsave all</a>\
                                            <a class='dropdown-item'  onclick=\"removeTopicUpdates({{ data.id }}, 'favorite');\">unlike all</a>\
                                            <div class='dropdown-divider'></div>\
                                            <a class='dropdown-item'  onclick=\"removeInterst('{{ data.id }}', 'topic-{{ data.id }}')\">remove</a>\
                                            {% if data.id|safe not in request.session.paused_topics %}\
                                                <a class='dropdown-item'  onclick=\"pauseTopic('{{ data.id }}')\">pause topic</a>\
                                            {% else %}\
                                                <a class='dropdown-item'  onclick=\"unpauseTopic('{{ data.id }}')\">unpause topic</a>\
                                            {% endif %}\
                                        </div>\
                        </div>\
                        <div class='card-body'>\
                            <div class='topic-stats'>\
                                <ul>\
                                    <li {% if data.read != 0 %} onclick=\"viewTopicsUpdates('{{ topic }}', 'read')\" {% endif %}>\
                                        <h5  id='topic-{{ data.id }}-read'>{{ data.read }}</h5>\
                                        read\
                                    </li>\
                                    <li {% if data.liked != 0 %} onclick=\"viewTopicsUpdates('{{ topic }}','favorite')\" {% endif %}>\
                                        <h5  id='topic-{{ data.id }}-favorite'>{{ data.liked }}</h5>\
                                        liked\
                                    </li>\
                                    <li {% if data.saved != 0 %} onclick=\"viewTopicsUpdates('{{ topic }}','saved')\" {% endif %}>\
                                        <h5 id='topic-{{ data.id }}-saved' >{{ data.saved }}</h5>\
                                        saved\
                                    </li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
    {% endfor %}\
")


def new(request):
    if request.method == 'POST':
        try:
            _string = request.POST.get('search-string-txt').lower()
            search_string = None
            if  _string != "*" and len(_string) != 0:
                response_message = ""
                if len(_string.split(" ")) < 3:
                    return HttpResponse("Topic not added. Please ensure your topic title contains 3 or more words.") 
                if not Search_String.objects.filter(string=_string).exists():
                    new_string = Search_String(string=_string)
                    new_string.save()
                else:
                    # the topic exists, we unfreez it if it was
                    Search_String.objects.filter(string=_string).update(is_frozen = False)
                # Add to the current user's list of strings
                user = User.objects.get(id = request.session.get('current_user').id)
                if len(user.search_strings) == 0:
                    user.search_strings = _string
                else:
                    if _string not in user.search_strings:
                        User.objects.filter(id = request.session.get('current_user').id).update(search_strings = user.search_strings + "|" + _string)
                    else:
                        response_message = "Topic already in your list."
                reload_user_settings(request)
                return HttpResponse(response_message)
        except Exception as e:
            print(e)
        return HttpResponseServerError("")
    return redirect(main)

def follow(request, arg, topic_id):
    fdback = "Topic successfully added!"
    if arg == "flw":
        search_strings = User.objects.get(id = request.session.get('current_user').id).search_strings.split("|")
        topic = Search_String.objects.get(id=topic_id).string
        if topic in search_strings:
            fdback = "You're already following this topic."
        else:
            search_strings.append(topic)
            User.objects.filter(id=request.session.get('current_user').id).update(search_strings = "|".join(search_strings) )
    elif arg == "uflw":
        remove_interest(request, topic_id)
        fdback = "Topic successfully removed"
    else:
        fdback = "Opps...Something went wrong. Please try again"
    return HttpResponse(fdback)


def remove_interest(request, topic_id):
    try:
        user = User.objects.get(id = request.session.get('current_user').id)
        users_string_list = [ topic for topic in user.search_strings.split("|") if topic != "" and Search_String.objects.get(string=topic).id != topic_id ]
        print(users_string_list)
        User.objects.filter(id = user.id).update(search_strings = "|".join(users_string_list))
        reload_user_settings(request)
        unpause(request, topic_id)
        return HttpResponse("")
    except Exception as e:
        print(e)
        return HttpResponseServerError()
    return HttpResponse("")


def get_all(request):
    return JsonResponse([ topic.string for topic in Search_String.objects.all() ], safe=False)


def get_topics(request):
    ''' Returns a view listing of the current user's topics '''
    user = User.objects.get(id = request.session.get('current_user').id)
    data = {}
    if len(user.search_strings) > 0:
        topics = [ topic for topic in user.search_strings.split("|") if topic != "" ]
        # print(topics)
        # topics = sorted(topics, key=str.lower, reverse=True)
        # print(topics)

        for topic in topics:
            data[topic if len(topic) < 25 else "{}...".format(topic[0:18])] = {"read":0, "liked":0, "saved": 0, "id": Search_String.objects.get(string=topic).id}


        if user.read_updates:
            for key_id in user.read_updates.split("|"):
                try:
                    update = Update.objects.get(key_id=key_id)
                    data[update.search_string.string]["read"] += 1
                except:
                    # The user has stopped tracking the topic while he already read one or more articles about it
                    print("ORPHAN UPDATE")

        if user.saved_updates:
            for key_id in user.saved_updates.split("|"):
                try:
                    update = Update.objects.get(key_id=key_id)
                    data[update.search_string.string]["saved"] += 1
                except:
                    # The user has stopped tracking the topic while he already saved one or more articles about it
                    print("ORPHAN UPDATE")

        if user.favorite_updates:
            for key_id in user.favorite_updates.split("|"):
                try:
                    update = Update.objects.get(key_id=key_id)
                    data[update.search_string.string]["liked"] += 1
                except:
                    # The user has stopped tracking the topic while he already liked one or more articles about it
                    print("ORPHAN UPDATE")

        user.paused_topics = user.paused_topics if user.paused_topics is not None else ""
        reload_user_settings(request)
        html = topics_cards.render(Context({"request": request,"topics":topics, "data":data}))
        return HttpResponse(html)
    return HttpResponse("No Topic saved yet")


def pause(request, topic_id):
    ''' a user decides to pause a topic for a moment
        the topic id will then be added to the list
        of paused topics and be discarded when displaying updates.
        ! This is only used to discard collected updates. It does not have
        leverage over which updates to collect or not.
    '''
    topic_id = str(topic_id)
    user = User.objects.get(id = request.session.get('current_user').id)
    if user.paused_topics is not None and len(user.paused_topics) + len(topic_id) + 1 > 255:
        return HttpResponseServerError("You cannot pause more topics. Please unpause one or more topics if you wish to carry on.")
    paused_topics = user.paused_topics.split("|") if user.paused_topics is not None else []
    paused_topics = [ topic for topic in paused_topics if topic != ''] # we make sure that paused topic won't have empty value
    if topic_id not in paused_topics:
        paused_topics.append(topic_id)
    a = "|".join(paused_topics)
    User.objects.filter(id = user.id).update(paused_topics = a)
    reload_user_settings(request)
    return HttpResponse("")


def unpause(request, topic_id):
    topic_id = str(topic_id)
    user = User.objects.get(id = request.session.get('current_user').id)
    paused_topics = user.paused_topics.split("|") if user.paused_topics is not None else []
    if topic_id in paused_topics:
        paused_topics.remove(topic_id)
    a = "|".join(paused_topics)
    User.objects.filter(id = user.id).update(paused_topics = a)
    reload_user_settings(request)
    return HttpResponse("")


def pause_all_other(request, topic_id):
    ''' this method will pause all topics except the one with id == topic_id '''
    topic_id = str(topic_id)
    user = User.objects.get(id = request.session.get('current_user').id)
    paused_topics = "|".join([ str(topic.id) for topic in Search_String.objects.filter(string__in = user.search_strings.split('|')).exclude(id = topic_id) ])
    print(paused_topics)
    User.objects.filter(id = user.id).update(paused_topics = paused_topics)
    reload_user_settings(request)
    return HttpResponse("")


def update_trending(request):
    ''' will count from the last 1000 updates
        and class accordingly
    '''
    latest = {}
    for update in Update.objects.all().order_by("-collection_date")[:1000]:
        if update.search_string.string not in latest:
            latest[update.search_string.string] = [1, update.search_string.id]
        else:
            latest[update.search_string.string][0] += 1
    top5 = []
    for topic, data in latest.items():
        top5.append([topic if len(topic)<37 else topic[:36] + "...", data[0], data[1]])
    top5 = sorted(top5, key= lambda e:e[1], reverse=True)[:5]
    t = Template("\
                    {% for top in top5 %}\
                        <li>\
                            <a>\
                                <p class='topic-title'>\
                                    {{ top.0 }}\
                                </p>\
                                <div class='t-count'>\
                                    <button type='button' id='btn-topic{{ top.2 }}-read' class='btn btn-light btn-sm' onclick=\"getTopicSpecificUpdate({{ top.2 }}, {{ top.1 }})\">{{ top.1 }} new updates</button>\
                                    <button type='button' id='btn-topic{{ top.2 }}-follow' onclick=\"followTopic('{{ top.2 }}')\" class='btn btn-light btn-sm'>\
                                        {% if top.0 in request.session.search_strings %}unfollow{% else %}follow this{% endif %}\
                                    </button>\
                                </div>\
                            </a>\
                        </li>\
                    {% endfor %}\
            ")
    request.session['trending_top5'] = top5
    return HttpResponse(t.render(Context({"top5": top5, "request":request})))


def update_popular(request):
    ''' this one loop through users subjects
    '''
    popular = {}
    total_usr_count = User.objects.all().count()
    for user in User.objects.all().exclude(search_strings = ""):
        for topic in [ topic for topic in user.search_strings.split("|") if topic !="" ]:
            if topic not in popular:
                popular[topic] = [1, Search_String.objects.get(string=topic).id]
            else:
                popular[topic][0] += 1
    top5 = []
    for topic, data in popular.items():
        top5.append([topic if len(topic) < 40 else topic[:37] + "...", int((data[0]*100)/total_usr_count), data[1]])
    top5 = sorted(top5, key= lambda e:e[1], reverse=True)[:5]
    # assert False
    t = Template("\
                    {% for top in top5 %}\
                        <li>\
                            <a>\
                                <p class='topic-title'>\
                                    {{ top.0 }}\
                                </p>\
                                <div class='t-count'>{{ top.1 }}% popular\
                                    <button type='button' id='btn-topic{{ top.2 }}-follow' onclick=\"followTopic('{{ top.2 }}')\" class='btn btn-light btn-sm'>\
                                            {% if top.0 in request.session.search_strings %}unfollow{% else %}follow this{% endif %}\
                                        </button>\
                                </div>\
                            </a>\
                        </li>\
                    {% endfor %}\
            ")
    request.session['popular_top5'] = top5
    return HttpResponse(t.render(Context({"top5": top5})))


