# coding=utf-8
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from app.models import *
from django.template import Template, Context
from django.http import HttpResponseServerError
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from cryptography.fernet import Fernet
import base64
from django.conf import settings
from django.db import IntegrityError

def index(request):
    error_message = ""
    if 'current_user' in request.session:#we update the user's data and redirect to main
        request.session['current_user'] = User.objects.get(id=request.session.get('current_user').id)
        return redirect(sign_in)
    if request.method == 'POST':
        try:
            login = request.POST.get('login')
            password = request.POST.get('password')
            validate_email(login)
            if User.objects.filter(email=login).exists():
                user = User.objects.get(email=login)
                cipher_suite = Fernet(settings.ENCRYPT_KEY)
                real_pass = base64.urlsafe_b64decode(user.access_string)
                real_pass = cipher_suite.decrypt(real_pass).decode("ascii")
                if real_pass == password:
                    request.session['current_user'] = user
                    return redirect(sign_in)
                else:
                    error_message = "Invalid password"
            else:
                error_message = "Unregistered email address. Please register by clicking on 'Sing Up'"
        except Exception as e:
            print(e)
    return render(request, 'index.html', {'error_message':error_message})

def register(request):
    if request.method == 'POST':
        try:
            login = request.POST.get('login')
            password = request.POST.get('password')
            validate_email(login)
            if password is None or password == "" or len(password) < 8:
                return HttpResponseServerError()            
            cipher_suite = Fernet(settings.ENCRYPT_KEY)
            encrypted_pass = cipher_suite.encrypt(password.encode('ascii'))
            encrypted_pass = base64.urlsafe_b64encode(encrypted_pass).decode("ascii")
            new_user = User(email=login, access_string=encrypted_pass)
            new_user.save()
            new_settings = Settings(user=new_user)
            new_settings.save()
            request.session['current_user'] = new_user
            return HttpResponse('')
        except IntegrityError:
            return HttpResponseServerError("Email already exists. Pleae sign-in instead")
        except Exception as e:
            print(e)
            return HttpResponseServerError()
    else:
        return HttpResponseServerError()

def sign_in(request):
    if type(request.session.get('current_user')) == User: 
        return HttpResponseRedirect("/dashboard/")
    else:
        return redirect(index)