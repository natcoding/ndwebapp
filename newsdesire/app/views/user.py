# coding=utf-8
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from app.models import *
from django.template import Template, Context
from django.http import HttpResponseServerError


def logout(request):
    del request.session['current_user']
    request.session.flush()
    return redirect("/")


