# coding=utf-8
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from app.models import *
from django.template import Template, Context
from datetime import datetime
import json
from app.views.public import index
from django.http import HttpResponseServerError


btn_status = {"content_type":"updates", "update_card_show_save_btn":True, "update_card_show_read_btn":True, "update_card_show_unsave_btn":False}

update_card = Template("\
            {% for update in updates %}\
                <div class='my-2' id='card-{{ update.key_id }}-{{ btn_status.content_type }}' name='{{ update.search_string.string }}'>\
                            <div class='card'>\
                                <div class='card-body'>\
                                    <h5 class='card-title' onclick=\"readUpdate('{{ update.key_id }}')\" style='cursor: pointer;'>{{ update.title|safe }}</h5>\
                                    <h6 class='card-subtitle mb-2 text-muted dropdown'>\
                                        <a class='dropdown-toggle mr-md-2' href='#' id='{{ update.key_id }}' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>\
                                            <strong class='pull-right'>{{ update.search_string.string|truncatechars:50 }}</strong>&nbsp;\
                                        </a>\
                                        <div class='dropdown-menu dropdown-menu-left' aria-labelledby='{{ update.key_id }}'>\
                                            <a {% if not is_focus %}class='dropdown-item' onclick=\"pauseAllOtherTopic({{ update.search_string.id }});\" {% else %} class='dropdown-item-text' {% endif %}>only this topic</a>\
                                            {% if  update.search_string.id|safe in request.session.current_user.paused_topics %}\
                                                <a id='lnk-pause-{{ update.search_string.id }}' class='dropdown-item' onclick=\"unpauseTopic({{ update.search_string.id }})\">unpause this topic</a>\
                                            {% else %}\
                                                <a id='lnk-pause-{{ update.search_string.id }}' class='dropdown-item' onclick=\"pauseTopic({{ update.search_string.id }})\">pause this topic</a>\
                                            {% endif %}\
                                            <div class='dropdown-divider'></div>\
                                            <a class='dropdown-item' onclick=\"removeInterst('{{ update.search_string.id }}', 'card-{{ update.key_id }}-{{ btn_status.content_type }}')\">delete this topic</a>\
                                        </div>\
                                        |&nbsp;&nbsp;{{ update.collection_date }}</h6>\
                                        <p class='card-text'>{{ update.text|safe }} {{ xxxxxxxxxxxxxxxxxxx }}</p>\
                                    {% if update.img_url %}\
                                        <img src='{{ update.img_url }}' class='rounded img-fluid'><br>\
                                    {% endif %}\
                                    <button type='button' id='{{ update.key_id }}-btn-save' \
                                        onclick=\"{% if update.key_id in request.session.saved_updates %}un{% endif %}saveUpdate('{{ update.key_id }}')\" class='btn btn-link'>\
                                        {% if update.key_id in request.session.saved_updates %}remove{% else %}save{% endif %}\
                                    </button>\
                                    {% if btn_status.update_card_show_read_btn %}\
                                        {% if update.key_id not in request.session.current_user.read_updates %}\
                                            <button id='btn-read-{{ update.key_id }}' type='button' onclick=\"readUpdate('{{ update.key_id }}')\" class='btn btn-link'>read</button><small id='{{ update.key_id }}-read-count'>{{ update.read_count }}</small>\
                                        {% else %}\
                                            <button id='btn-read-{{ update.key_id }}'id='btn-read-{{ update.key_id }}' type='button' onclick=\"readUpdate('{{ update.key_id }}')\" class='btn btn-link'>read again</button><small id='{{ update.key_id }}-read-count'>{{ update.read_count }}</small>\
                                        {% endif %}\
                                    {% endif %}\
                                    {% if update.key_id in request.session.current_user.favorite_updates %}\
                                        <button id='heart-{{ update.key_id }}' onclick=\"likeUpdate('{{ update.key_id }}')\" type='button' class='btn btn-link'><i class='fas fa-heart'></i></button><small  id='{{ update.key_id }}-like-count'>{{ update.like_count }}</small>\
                                    {% else %}\
                                        <button id='heart-{{ update.key_id }}' type='button' onclick=\"likeUpdate('{{ update.key_id }}')\" class='btn btn-link'><i class='far fa-heart'></i></button><small id='{{ update.key_id }}-like-count'>{{ update.like_count }}</small>\
                                    {% endif %}\
                                </div>\
                            </div>\
                        </div>\
            {% endfor %}\
            ")


def main(request):
    if 'current_user' in request.session:
        reload_user_settings(request)
        try:
            del request.session['latest_newest_collected']
            del request.session['latest_oldest_collected']
            request.session['trending_top5'] = []
            request.session['popular_top5'] = []
        except:
            pass
        return render(request, 'dashboard.html', {})
    return redirect(index)


def reload_user_settings(request):
    # The need to save user update and topic settings in session
    # lists instead of relying on their object form.
    user = User.objects.get(id = request.session.get('current_user').id)
    request.session['current_user'] = user
    a = user.search_strings
    request.session['search_strings'] = a
    request.session['saved_updates'] = user.saved_updates.split("|") if user.saved_updates is not None else ""
    request.session['paused_topics'] = user.paused_topics.split("|") if user.paused_topics is not None else ""

def is_focus(request):
    ''' returns true if user is only tracking one of
        all his topics, with total topics > 2
    '''
    b = False
    try:
        user = User.objects.get(id = request.session.get('current_user').id)
        total_topics = len(user.search_strings.split("|"))
        if total_topics > 2 and len(user.paused_topics.split("|")) == total_topics -1:
            b = True
    except:
        pass
    return b



def get_unloaded_update_count(request):
    return HttpResponse(request.session.get('count_unloaded_updates') )


def load_latest(request):
    ''' SHould later use the user's registered strings '''
    btn_status["content_type"] = "updates"
    btn_status["update_card_show_save_btn"] = True
    btn_status["update_card_show_unsave_btn"] = False
    updates = None
    user = User.objects.get(id = request.session.get('current_user').id)
    users_string_list = [ topic for topic in user.search_strings.split("|") if topic != "" ]
    paused_topics = [ str(topic) for topic in user.paused_topics.split("|") if topic != "" ] if user.paused_topics else ""
    if len(users_string_list) != 0:
        if type(request.session.get("latest_newest_collected")) == Update:
            last_up = request.session.get("latest_newest_collected")
            updates = list(Update.objects.filter(search_string__string__in = users_string_list, collection_date__gt = last_up.collection_date, language=user.settings.pref_lang).exclude(search_string__id__in = paused_topics).exclude(url=last_up.url).order_by("-collection_date"))
        else:
            updates = Update.objects.filter(search_string__string__in = users_string_list, language=user.settings.pref_lang).exclude(search_string__id__in = paused_topics).order_by("-collection_date")[:15]
            # assert False
            try:
                request.session["latest_newest_collected"] = updates[0]
                request.session["latest_oldest_collected"] = updates[len(updates)-1]
            except Exception as e:
                print(e)
        if len(updates) == 0:
            request.session['count_unloaded_updates'] = 0
            return HttpResponse("None")

        html = update_card.render(Context({'updates': updates, 'btn_status':btn_status, 'request': request, "is_focus": is_focus(request)}))
        return HttpResponse(html)
    return HttpResponse("None")


def load_specific(request, topic_id, q):
    updates = Update.objects.filter(search_string__id = topic_id, language=user.settings.pref_lang).order_by("-collection_date")[:q]
    html = update_card.render(Context({'updates': updates, 'btn_status':btn_status, 'request': request, "is_focus": is_focus(request)}))
    return HttpResponse(html)

def clear_latest_newest_collected(request):
    try:
        del request.session['latest_newest_collected']
    except:
        pass
    return redirect(load_latest)


def load_more(request):
    last_up = request.session.get("latest_oldest_collected")
    user = User.objects.get(id = request.session.get('current_user').id)
    users_string_list = user.search_strings.split("|")
    paused_topics = [str(topic) for topic in user.paused_topics.split("|")] if len(user.paused_topics) != 0 else None
    updates = []
    try:
        if paused_topics is not None:
            updates = list(Update.objects.filter(search_string__string__in = users_string_list, language=user.settings.pref_lang).exclude(search_string__id__in = paused_topics).filter(collection_date__lte = last_up.collection_date).exclude(url=last_up.url).order_by("-collection_date")[:4+1])
        else:
            updates = list(Update.objects.filter(search_string__string__in = users_string_list, collection_date__lte = last_up.collection_date, language=user.settings.pref_lang).exclude(url=last_up.url).order_by("-collection_date")[:4+1])
    except:
        pass
    if len(updates) != 0:
        btn_status["content_type"] = "updates"
        btn_status["update_card_show_save_btn"] = True
        btn_status["update_card_show_unsave_btn"] = False
        html = update_card.render(Context({'updates': updates, 'btn_status':btn_status, 'request': request, "is_focus": is_focus(request)}))
        request.session["latest_oldest_collected"] = updates[len(updates)-1]
        return HttpResponse(html)
    else:
        return HttpResponse("None")


def read(request, key_id):
    update = Update.objects.get(key_id=key_id)
    count = update.read_count + 1
    user = User.objects.get(id = request.session.get('current_user').id)
    read_up_string = user.read_updates if user.read_updates else ""
    if key_id not in user.read_updates:
        if len(read_up_string) == 0:
            read_up_string = key_id
        else:
            read_up_string = read_up_string + "|" + key_id
        User.objects.filter(id = request.session.get('current_user').id).update(read_updates = read_up_string)
    Update.objects.filter(key_id=key_id).update(read_count=count)
    reload_user_settings(request)
    return HttpResponse(update.url)


def save(request, key_id):
    response_message = "Update already saved!"
    update = Update.objects.get(key_id=key_id)
    user = User.objects.get(id = request.session.get('current_user').id)
    saved_up_string = user.saved_updates if user.saved_updates else ""
    if key_id not in saved_up_string:
        if len(saved_up_string) == 0:
            saved_up_string = key_id
        else:
            saved_up_string = saved_up_string + "|" + key_id
        User.objects.filter(id = request.session.get('current_user').id).update(saved_updates=saved_up_string)
        response_message = ""
    reload_user_settings(request)
    return HttpResponse(response_message)


def unsave(request, key_id):
    user = User.objects.get(id = request.session.get('current_user').id)
    saved_up_string = user.saved_updates if user.saved_updates else ""
    if key_id not in saved_up_string:
        return HttpResponseServerError()
    else:
        if "{}|".format(key_id) in saved_up_string:
            saved_up_string = saved_up_string.replace("{}|".format(key_id),"")
        else:
            saved_up_string = saved_up_string.replace("{}".format(key_id),"")
        User.objects.filter(id=user.id).update(saved_updates=saved_up_string)
        reload_user_settings(request)
        return HttpResponse()


def like(request, key_id):
    like_count = None
    update = Update.objects.get(key_id=key_id)
    user = User.objects.get(id = request.session.get('current_user').id)
    fav_up_string = user.favorite_updates if user.favorite_updates  else ''
    # fav_up_string = user.favorite_updates
    if key_id not in fav_up_string:
        like_count = update.like_count + 1 #increase like count only if liking
        if len(fav_up_string) == 0:
            fav_up_string = key_id
        else:
            fav_up_string = fav_up_string + "|" + key_id
        User.objects.filter(id = request.session.get('current_user').id).update(favorite_updates=fav_up_string)
        # save(request, key_id) # since a liked update is also saved
    else:# anticipate the second click, we unlike
        like_count = update.like_count - 1 #decrease like count
        user = User.objects.get(id = request.session.get('current_user').id)
        favorite_updates = user.favorite_updates.split("|")
        favorite_updates.remove(key_id)
        User.objects.filter(id = user.id).update(favorite_updates = "|".join(favorite_updates))
        # unsave(request, key_id)
    
    Update.objects.filter(key_id=key_id).update(like_count=like_count)
    reload_user_settings(request)
    return HttpResponse()


def get_total_saved(request):
    user = User.objects.get(id=request.session.get('current_user').id)
    updates = user.saved_updates.split("|")
    if updates[0] == "":
        return HttpResponse(0)
    return HttpResponse(len(user.saved_updates.split("|")))


def display_saved_updates(request):
    user = User.objects.get(id=request.session.get('current_user').id)
    saved_updates = Update.objects.filter(key_id__in = user.saved_updates.split("|")).order_by("-collection_date")
    if len(saved_updates) == 0:
        return HttpResponse("void")
    else:
        btn_status["content_type"] = "saved"
        btn_status["update_card_show_save_btn"] = False
        btn_status["update_card_show_unsave_btn"] = True
        html = update_card.render(Context({'updates': saved_updates, 'btn_status':btn_status, "request":request, "is_focus": is_focus(request)}))
        return HttpResponse(html)



def get_update_by_label(request, topic, label):
    ''' Returns labelled updates. Label can be all, read, favorite, saved depending
        the connected user
    '''
    topic = topic.replace("%20", " ")
    request.session['current_user'] = User.objects.get(id = request.session.get('current_user').id)
    key_list = request.session.get("current_user").__dict__["{}_updates".format(label)].split("|")

    updates = Update.objects.filter(key_id__in = key_list).filter(search_string__string = topic)
    html = update_card.render(Context({'updates': updates, 'btn_status':btn_status, 'request': request, "is_focus": is_focus(request)}))
    if key_list[0]:
        return HttpResponse(html)
    else:
        return HttpResponse(" ")


def remove_update_by_label(request, topic_id, label):
    ''' Removes update matching the filter topic_id:label '''
    request.session['current_user'] = User.objects.get(id = request.session.get('current_user').id)
    key_list = request.session.get("current_user").__dict__["{}_updates".format(label)].split("|")
    print(key_list)
    key_list = [key for key in key_list if Update.objects.get(key_id=key).search_string.id != int(topic_id)]
    # for key in key_list:
    #     print("Another key")
    #     if Update.objects.get(key_id=key).search_string.id == int(topic_id):
    #         key_list.remove(key)
    print(key_list)
    # we update the user's list of $label updates
    if label == "saved":
        User.objects.filter(id = request.session.get('current_user').id).update(saved_updates = "|".join(key_list))
    elif label == "favorite":
        User.objects.filter(id = request.session.get('current_user').id).update(favorite_updates = "|".join(key_list))
    return HttpResponse("")









