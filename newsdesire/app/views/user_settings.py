# coding=utf-8
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from app.models import *
from django.template import Template, Context
from datetime import datetime
from django.http import HttpResponseServerError, JsonResponse
from app.views.dashboard import reload_user_settings
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize
import json
from ast import literal_eval

def home(request):
    return render(request, 'settings.html', {})


def get_all(request):
    settings = literal_eval(serialize("json",[get_user_settings(request),]))[0].get("fields")
    return JsonResponse(settings)

def get_user_settings(request):
    user = User.objects.get(id=request.session.get("current_user").id)
    if not user.settings:
        new_settings = Settings()
        new_settings.save()
        User.objects.filter(id=user.id).update(settings = new_settings)
        return new_settings
    return user.settings

@csrf_exempt
def update_settings(request):
    settings = get_user_settings(request)
    if request.method == "POST":
        try:
            for key in request.POST.keys():
                value = request.POST.get(key)
                if key == "pref_lang":
                    Settings.objects.filter(id=settings.id).update(pref_lang=value)
        except Exception as e:
            print(e)
            return HttpResponseServerError()
    return HttpResponse()

