from django.db import models


class Search_String(models.Model):
    string = models.CharField(max_length=255, unique=True)
    reg_date = models.DateTimeField(auto_now=True)
    is_frozen = models.BooleanField(default=False)


class Update(models.Model):
    url = models.URLField(max_length=2000)
    collection_date = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=200)
    text = models.CharField(max_length=400, null=True)
    search_string = models.ForeignKey(Search_String,null=True, on_delete=models.SET_NULL)
    read_count = models.IntegerField(default=0)
    key_id = models.CharField(max_length=10, null=False, primary_key=True)
    img_url = models.URLField(max_length=2000, null=True)
    like_count = models.IntegerField(default=0) # how many times the update was liked
    language = models.CharField(max_length=2, default="en")

    def __str__(self):
        return 'Key {}: {}'.format(self.key_id, self.search_string.string)


class Settings(models.Model):
    pref_lang = models.CharField(max_length=2, default="en")

class User(models.Model):
    email = models.EmailField(unique=True, null=False, blank=False)
    access_string = models.CharField(max_length=250)
    search_strings = models.CharField(max_length=1250) #commas separated string values packed into a list
    saved_updates = models.CharField(max_length=5250) #commas separated key_id values packed into a list
    read_updates = models.CharField(max_length=5250) #commas separated key_id values packed into a list
    favorite_updates = models.CharField(max_length=5250, null=True) #commas separated key_id values packed into a list
    paused_topics = models.CharField(max_length=255, null=True) #commas separated int values packed into a list
    date_joined = models.DateField(auto_now=True)
    settings = models.OneToOneField(Settings, primary_key=False, on_delete=models.CASCADE, null=True)


