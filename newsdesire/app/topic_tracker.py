''' THis script has for sole purpose of tracking the topics by
    doing some checkings like verifying if a topic is still
    tracked by any user and performs relevant actions... '''

from app.models import Search_String, User, Settings

def freezer():
    total = Search_String.objects.all().count()
    frozen = 0
    for topic in Search_String.objects.all():
        is_frozen = True
        # print("Working on \"{}\"".format(topic.string))
        for user in User.objects.all():
            if topic.string in user.search_strings:
                is_frozen = False
                Search_String.objects.filter(id=topic.id).update(is_frozen=is_frozen)
                break
        if is_frozen:
            Search_String.objects.filter(id=topic.id).update(is_frozen=True)
            frozen += 1
        else:
            continue
    report = "* Freezer() Report=>Toal:    {}|Frozen:    {}    *".format(total, frozen)
    wrapper = "*" * len(report)
    print(wrapper)
    print(report)
    print(wrapper)


def get_users_languages():
    ''' The goal of this function is to return a list of 
        users' prefered languages to be used when collecting
        updates
    '''
    lang_list = ["en"]
    for user in User.objects.all():
        if user.settings is not None and user.settings.pref_lang not in lang_list:
            lang_list.append(user.settings.pref_lang)
        elif user.settings is None or user.settings.pref_lang is None:
            new_settings = Settings()
            new_settings.save()
            User.objects.filter(id = user.id).update(settings = new_settings)
    return lang_list


pass