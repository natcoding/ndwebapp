from django.contrib import admin
from django.urls import path, re_path
from app.views import *

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', public.index),
    path('dashboard/', dashboard.main),
    path('search-string/new/', topic.new),

    #'''    UPDATES '''
    path('update/count/unloaded/', dashboard.get_unloaded_update_count),
    path('update/count/saved/', dashboard.get_total_saved),
    path('update/get/latest/', dashboard.load_latest),
    path('update/get/specific/<int:topic_id>/<int:q>/', dashboard.load_specific),
    path('update/get/more/', dashboard.load_more),
    path('update/get/newest/', dashboard.clear_latest_newest_collected),
    re_path(r'^update/read/(?P<key_id>[a-zA-Z0-9]*)/$', dashboard.read),
    re_path(r'^update/save/(?P<key_id>[a-zA-Z0-9]*)/$', dashboard.save),
    re_path(r'^update/unsave/(?P<key_id>[a-zA-Z0-9]*)/$', dashboard.unsave),
    re_path(r'^update/like/(?P<key_id>[a-zA-Z0-9]*)/$', dashboard.like),
    path('update/get/saved/', dashboard.display_saved_updates),
    re_path(r'update/get/topic/(?P<topic>.*)/labelled/(?P<label>[a-z]*)/', dashboard.get_update_by_label),
    re_path(r'update/remove/topic/(?P<topic_id>[0-9]*)/labelled/(?P<label>[a-z]*)/', dashboard.remove_update_by_label),

    #''' USER'S '''
    path('usr/register/', public.register),
    path('usr/sign-in/', public.sign_in),
    path('usr/sign-out/', user.logout),


    #''' TOPICS and search strings are actuall the same thing!!!'''
    path('interst/get/all', topic.get_all),
    path('interest/mine/remove/<int:topic_id>/', topic.remove_interest),
    path('interst/mine/get/all', topic.get_topics),
    path('interst/pause/<int:topic_id>/', topic.pause),
    path('interst/unpause/<int:topic_id>/', topic.unpause),
    path('interst/pause-all-other/<int:topic_id>/', topic.pause_all_other),
    path('interst/trending/', topic.update_trending),
    path('interst/popular/', topic.update_popular),
    path('interst/<str:arg>/<int:topic_id>/', topic.follow),


    #''' USER'S SETTINGS ''/settings/update/pref-lang
    path('settings/', user_settings.home),
    path('settings/get/all', user_settings.get_all),
    path('settings/update/', user_settings.update_settings),
]
